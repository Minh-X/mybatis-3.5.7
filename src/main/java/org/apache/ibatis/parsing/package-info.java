/**
 *    Copyright 2009-2020 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
/**
 * Parsing utils.
 */
package org.apache.ibatis.parsing;

/*
解析器模块主要可查看以下内容

1. 概述
      一个功能，是对 XPath 进行封装，为 MyBatis 初始化时解析 mybatis-config.xml 配置文件以及映射配置文件提供支持。
      另一个功能，是为处理动态 SQL 语句中的占位符提供支持

2. XPathParser
2.1 构造方法
2.2 eval 方法族
2.2.1 eval 元素
2.2.2 eval 节点
3. XMLMapperEntityResolver
4. GenericTokenParser
5. PropertyParser
6. TokenHandler
6.1 VariableTokenHandler
6.1.1 构造方法
6.1.2 handleToken

 */
